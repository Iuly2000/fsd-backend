import { Request, Response, NextFunction } from "express";

let jwt = require("jsonwebtoken");

export const authJWT = (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split(' ')[1];

    jwt.verify(token, process.env.SECRET_KEY, (err: any, object: any) => {
      if (err) {
        return res.sendStatus(401);
      }

      req.body = object;
      return next();
    });
  } else {
    res.sendStatus(401);
  }
};
