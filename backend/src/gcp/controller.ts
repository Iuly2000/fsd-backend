import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { Schedule } from "../entity/Schedule";

// Imports the Google Cloud client library
const { Datastore } = require('@google-cloud/datastore');

// Creates a client
const datastore = new Datastore();

// The kind for the new entity
const kind = 'schedule';

export class scheduleController {

  async create(request: Request, response: Response, next: NextFunction) {
    const { start_date, end_date } = request.body;
    response.setHeader("Access-Control-Allow-Origin", "*");
    if (start_date && end_date) {

      // The Cloud Datastore key for the new entity
      const scheduleKey = datastore.key(kind);

      const schedule = {
        key: scheduleKey,
        data: {
          end_date: end_date,
          start_date:start_date
        },
      };

      await  datastore.save(schedule);
      response.status(200);
      return "Successfully created schedule";
    }
    response.status(500);
    return "Invalid data";
  }

  async selectAll(request: Request, response: Response, next: NextFunction) {
    const query = datastore.createQuery('schedule');    
    response.setHeader("Access-Control-Allow-Origin", "*");
    const [schedules] = await datastore.runQuery(query);
    return schedules;
  }
}

