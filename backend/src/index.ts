import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Routes } from "./routes/routes";

createConnection()
    .then(async (connection) => {
        // create express app
        const fileUpload = require('express-fileupload');
        const app = express();
        app.use(bodyParser.json());
        app.use(fileUpload({
            createParentPath: true
          }));
        const router = express.Router();

        // register express routes from defined application routes
        Routes.forEach((route) => {
            //if present, register middlewares in the order they were given
            if (route.middleware) {
                (router as any)[route.method](route.route, route.middleware);
            }
            (router as any)[route.method](
                route.route,
                (req: Request, res: Response, next: Function) => {
                    const result = new (route.controller as any)()[route.action](
                        req,
                        res,
                        next
                    );
                    if (result instanceof Promise) {
                        result.then((result) =>
                            result !== null && result !== undefined
                                ? res.json(result)
                                : undefined
                        );
                    } else if (result !== null && result !== undefined) {
                        res.json(result);
                    }
                }
            );
        });

        app.use("/", router);

        const result = require("dotenv").config();

        if (result.error) {
            throw result.error;
        }

        // start express server
        app.listen(3001);
    })
    .catch((error) => console.log(error));

