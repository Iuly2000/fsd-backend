import { UserController } from "../controllers/userController";
import { scheduleController } from "../gcp/controller";
import { authJWT } from "../middleware/authenticate";
import { TensorFlowController } from "../controllers/TensorFlowController";

export const Routes = [{
   method: "get",
   route: "/users",
   controller: UserController,
   action: "all",
   middleware: authJWT
}, {
   method: "get",
   route: "/users/:id",
   controller: UserController,
   action: "one",
   middleware: authJWT
}, {
   method: "post",
   route: "/users",
   controller: UserController,
   action: "save",
   middleware: authJWT
}, {
   method: "delete",
   route: "/users/:id",
   controller: UserController,
   action: "remove",
   middleware: authJWT
}, {
   method: "get",
   route: "/login",
   controller: UserController,
   action: "login",
}, {
   method: "post",
   route: "/register",
   controller: UserController,
   action: "register",
}, {
   method: "get",
   route: "/schedules",
   controller: scheduleController,
   action: "selectAll"
}, {
   method: "post",
   route: "/addedSchedule",
   controller: scheduleController,
   action: "create"
},
{
  method: "post",
  route: "/train",
  controller: TensorFlowController,
  action: "train",
},

{
   method: "post",
   route: "/evaluate",
   controller: TensorFlowController,
   action: "evaluate"
}

];