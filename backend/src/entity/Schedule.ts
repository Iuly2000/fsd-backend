import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("schedule")
export class Schedule {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    end_date: Date;

    @Column()
    start_date: Date;
}