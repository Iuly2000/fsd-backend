import { NextFunction, Request, Response } from "express";
import { MnistData } from '../trainModel/data.js';
import { getModel, train, doPrediction } from '../trainModel/model.js';

export class TensorFlowController {


  async evaluate(request, response: Response, next: NextFunction) {
    try {
      if (!request.files) {
        response.send({
          status: false,
          message: 'Error: No file uploaded'
        });
      } else {
        let arrayBuffer = this.toArrayBuffer(request.files.myFile.data);
        return doPrediction(arrayBuffer);

      }
    } catch (err) {
      response.json({ Error: "Error while uploading file." })
    }

  }

  async train(request: Request, response: Response, next: NextFunction) {
    const data = new MnistData();
    await data.load();

    const model = await train(getModel(), data);
    await model.save("file://./src/trainModel/model");

    return model;
  }

  toArrayBuffer(buf) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
      view[i] = buf[i];
    }
    return ab;
  }

}



