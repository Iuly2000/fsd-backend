import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";

const jwt = require('jsonwebtoken')

export class UserController {

    private userRepository = getRepository(User);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        await this.userRepository.remove(userToRemove);
    }

    async register(request: Request, response: Response, next: NextFunction) {
        const { email, password, userName } = request.body;

        if (userName && email && password) {
            let userExists = (await this.userRepository.find({
                    where:
                        [
                            { userName: request.body.userName },
                            { email: request.body.email },
                        ],
                })
            ).length > 0;

            if (userExists) {
                response.status(409);
                return "User already exists";
            }
            await this.userRepository.save(request.body);
            return "Successful Register";
        }
        response.status(401);
        return "Invalid data";
    }

    async login(request: Request, response: Response, next: NextFunction) {
        const { email, password } = request.body;

        if (email && password) {
            let user = (
                await this.userRepository.find({
                    where: { email: email, password: password },
                })
            ).shift();
            
            if (!user) {
                response.status(401);
                return "Incorrect credentials";
            }
            response.status(200);
            return { token: jwt.sign(user.id, process.env.SECRET_KEY) };
        }
        response.status(401);
        return "Invalid data";
    }
}
